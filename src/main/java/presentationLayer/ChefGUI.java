package presentationLayer;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

public class ChefGUI implements Observer {
	
	public ChefGUI() {
	}
	
	public void update(Observable arg0, Object arg1) {
		JOptionPane.showMessageDialog(null, "Order sent to the Chef", "Chef", JOptionPane.PLAIN_MESSAGE, null);
	}
	

}
