package businessLayer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem
{
	
	private ArrayList<MenuItem> items;
	
	public CompositeProduct(String name, ArrayList<MenuItem> menuItems)
	{
		super(name, 0);
		this.items = menuItems;
		double price = 0;
		for(MenuItem currentMenuItem : menuItems)
		{
			price += currentMenuItem.getPrice();
		}
		super.setPrice(price);
	}

	public ArrayList<MenuItem> getItems() 
	{
		return items;
	}
	
	
}
