package businessLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

import presentationLayer.ChefGUI;

/**
 * Class Restaurant is the class which contains all the possible commands that can be executed by the administrator or the waiter
 * @author mihai
 *
 */
public class Restaurant extends Observable implements IRestaurantProcessing, Serializable
{
	private ArrayList<MenuItem> menuItem;
	private HashMap<Order, ArrayList<MenuItem>> placedOrders;
	

	/**
	 * Constuctor for a new Restaurant Object
	 * Besides initialising the Restaurant's attributes, it also creates a new GUI which notifies the Chef everytime an order is placed
	 */
	public Restaurant()
	{
		this.menuItem = new ArrayList<MenuItem>();
		this.placedOrders = new HashMap<Order, ArrayList<MenuItem>>();
		this.addObserver(new ChefGUI());
	}


	/**
	 * Getter for the list of items
	 * @return returns the list of menu items
	 */
	public ArrayList<MenuItem> getMenuItem() 
	{
		return menuItem;
	}


	/**
	 * Getter for the HashMap of orders
	 * @return returns the HashMap of placed orders
	 */
	public HashMap<Order, ArrayList<MenuItem>> getPlacedOrders() 
	{
		return placedOrders;
	}


	/**
	 * Method for adding a new menuItem to the list of menuItems
	 * @param newMenuItem represents the new menuItem that will be added to the list
	 */
	public void createMenuItem(MenuItem newMenuItem)
	{
		assert newMenuItem != null;
		assert newMenuItem.computePrice() > 0;
		menuItem.add(newMenuItem);
		assert this.menuItem != null;
		
	}

	/**
	 * Method for deleting a menuItem from the lost of menuItems
	 * @param deletedMenuItemName represents the name of the menuItem that will be deleted
	 */
	public void deleteMenuItem(String deletedMenuItemName)
	{
		assert deletedMenuItemName != null;
		assert menuItem != null;
		MenuItem deletedMenuItem = new MenuItem(deletedMenuItemName, 0);
		for(int i = 0; i < menuItem.size(); i++)
		{
			if(menuItem.get(i).equals(deletedMenuItem) == true)
				menuItem.remove(menuItem.get(i));
		}
		
	}


	/**
	 * Method for updating the price of a menuItem from our menuItems list
	 * @param editedMenuItem represents the menuItem that will have its price updated
	 * @param newPrice represents the newPrice that will replace the old price of our chosen menuItem
	 */
	public void editMenuItem(MenuItem editedMenuItem, double newPrice)
	{
		assert editedMenuItem != null;
		assert newPrice > 0;
		for(int i = 0; i < menuItem.size(); i++)
		{
			if(menuItem.get(i).equals(editedMenuItem) == true)
			{
				menuItem.get(i).setPrice(newPrice);
			}
		}
		assert editedMenuItem.getPrice() == newPrice;
	}


	/**
	 * Method for creating a new Order, the Chef is also notified everytime a new Order is being placed
	 * @param newOrder represents the new order that will be added to our HashMap of placed orders
	 * @param menuItem represents the list of menuItems that form the new order
	 */
	public void createNewOrder(Order newOrder, ArrayList<MenuItem> menuItem) 
	{
		assert newOrder != null;
		assert menuItem != null;
		placedOrders.put(newOrder, menuItem);
		assert placedOrders != null;
		new ChefGUI();
		this.setChanged();
		this.notifyObservers();
	}


	/**
	 * Method that computes the total price of an order by adding the prices of all its individual menuItems
	 * @param currentOrder represents the order which will have its price computed
	 * @return returns the double value representing the total price of the order
	 */
	public double computeOrderPrice(Order currentOrder)
	{
		assert currentOrder != null;
		double totalPrice = 0;
		assert placedOrders.get(currentOrder) != null;
		for(MenuItem menuItem : placedOrders.get(currentOrder)) {
			totalPrice += menuItem.computePrice();
		}
		assert totalPrice > 0;
		return totalPrice;
	}


	/**
	 * Method for generating an order's bill in .txt format
	 * @param currentOrder represents the order for which the bill will be generated
	 * @param outputFile represents the String Object which will form the title of the bill, together with the order's id
	 * @param orderID represents the order's id
	 */
	public void generateBill(Order currentOrder,String outputFile, int orderID)
	{
		assert currentOrder != null;
		assert orderID > 0;
		assert outputFile != null;
		String fileName = outputFile + orderID + ".txt" ;
		
		try(PrintWriter pw = new PrintWriter(fileName))
		{
			pw.println();
			pw.println("\t~MyRestaurantSRL~");
			pw.println("Date : " + currentOrder.getDate());
			pw.println("Table Number : " + currentOrder.getTable());
			
			pw.println("Ordered items : ");
			for(MenuItem menuItem : placedOrders.get(currentOrder))
			{
				pw.println("\t" + menuItem.getName() + "," + menuItem.getPrice() + "$");
			}
			pw.println("TOTAL : " + computeOrderPrice(currentOrder) + "$");
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
	}
	
	
	
}
