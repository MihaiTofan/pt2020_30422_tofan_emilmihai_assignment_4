package businessLayer;

import java.io.Serializable;

public class MenuItem implements Serializable
{
	
	private String name;
	private double price;
	
	
	public MenuItem()
	{
		
	}
	
	public MenuItem(String name, double price)
	{
		this.name = name;
		this.price = price;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price) 
	{
		this.price = price;
	}
	
	public double computePrice() {
		return price;
	}
	
	public boolean equals(MenuItem m)
	{
		if(m.getName().equals(this.name))
		{
			return true;
		} 
		else return false;
	}
}
