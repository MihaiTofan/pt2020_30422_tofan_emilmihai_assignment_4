package businessLayer;

import java.util.ArrayList;

public interface IRestaurantProcessing
{
	/**
	 * @pre newMenuItem !=null
	 * @pre newMenuItem.computePrice() > 0
	 * @post menuItem != null
	 * @param newMenuItem represents the new MenuItem that will be added to our list
	 */
	public void createMenuItem(MenuItem newMenuItem);
	
	/**
	 * @pre deletedMenuItem !=null
	 * @pre menuItem !=null
	 * @param deletedMenuItemName represents the name of the menuItem that will be deleted
	 */
	public void deleteMenuItem(String deletedMenuItemName);
	
	
	/**
	 * @pre editedMenuItem != null
	 * @pre newPrice > 0
	 * @post editedMenuItem.getPrice() == newPrice
	 * @param editedMenuItem represents the menuItem that will have its price updated
	 * @param newPrice represents the newPrice that will replace the old price of our chosen menuItem
	 */
	public void editMenuItem(MenuItem editedMenuItem, double newPrice);
	
	/**
	 * @pre newOrder != null
	 * @pre menuItem != null
	 * @post placedOrders != null
	 * @param newOrder represents the new order that will be added to our HashMap of placed orders
	 * @param menuItem represents the list of menuItems that form the new order
	 */
	public void createNewOrder(Order newOrder, ArrayList<MenuItem> menuItem);
	
	/**
	 * @pre currentOrder != null
	 * @pre placedOrders.get(currentOrder) != null
	 * @post totalPrice > 0
	 * @param currentOrder represents the order which will have its price computed
	 * @return returns the double value representing the total price of the order
	 */
	public double computeOrderPrice(Order currentOrder);
	
	/**
	 * @pre currentOrder != null
	 * @pre orderID > 0
	 * @pre outputFile != null
	 * @param currentOrder represents the order for which the bill will be generated
	 * @param outputFile represents the String Object which will form the title of the bill, together with the order's id
	 * @param orderID represents the order's id
	 */
	public void generateBill(Order currentOrder,String outputFile, int orderID);
}
